const express = require("express");
const mongoose = require("mongoose");
const taskRoutes = require('./routes/taskRoutes')

const app = express();
const port = 4000;


app.use(express.json());
app.use(express.urlencoded({extended:true}));

mongoose.connect('mongodb+srv://admin:admin@prodmongodb.iaelz.mongodb.net/course-booking?retryWrites=true&w=majority', {

				useNewUrlParser : true,  
				useUnifiedTopology : true
			}
		);

let db = mongoose.connection; 

db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("Successfully Connected"));

app.use('/tasks', taskRoutes)


app.listen(port, () => console.log(`Server running at port ${port}`));
