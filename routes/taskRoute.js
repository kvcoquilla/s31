const express = require('express')
const router = express.Router()
const taskController = require('../controllers/taskControllers')

router.get('/', (req, res) => {

	taskController.getAllTasks().then(resultFromController => res.send(resultFromController))
})

//Activity Start
router.get('/tasks/:id', (req,res) => {
	taskController.getSpecificTasks(req.params.id).then(resultFromController => res.send(resultFromController))
})

router.put('/tasks/:id/complete', (req,res) => {
	taskController.updateToComplete(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})
//Activity End

router.post('/createTask', (req, res) => {

	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController))
})

router.delete('/deleteTask/:id', (req, res) => {

	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))
})


router.put('/updateTask/:id', (req, res) => {

	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})

module.exports = router

